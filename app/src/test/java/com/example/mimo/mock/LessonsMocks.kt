package com.example.mimo.mock

import com.example.mimo.domain.Content
import com.example.mimo.domain.Lesson
import com.example.mimo.domain.Lessons

object LessonsMocks {

    val allLessonsAreResponded = Lessons(
        listOf(
            Lesson(
                1,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")
                ),
                null, true
            ),
            Lesson(
                2,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")
                ),
                null, true
            )
        )
    )

    val firstLessonsIsResponded = Lessons(
        listOf(
            Lesson(
                1,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, true
            ),
            Lesson(
                2,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, false
            ),
            Lesson(
                3,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, false
            )
        )
    )

    val allLessonsAreNotResponded = Lessons(
        listOf(
            Lesson(
                1,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, false
            ),
            Lesson(
                2,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, false
            ),
            Lesson(
                3,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, false
            )
        )
    )

    val lastLessonsIsNotResponded = Lessons(
        listOf(
            Lesson(
                1,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, true
            ),
            Lesson(
                2,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, true
            ),
            Lesson(
                3,
                listOf(
                    Content("#000000", "Hello"),
                    Content("#000000", "World")),
                null, false
            )
        )
    )

}