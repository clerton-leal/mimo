package com.example.mimo.mock

import com.example.mimo.domain.*

object LessonMocks {

    val lessonWithoutInput = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#000000", "World")
        ),
        null,
        true
    )

    val lessonPartsWithoutInput = listOf(
        LessonPart("#000000", "Hello", LessonPartType.TEXT),
        LessonPart("#000000", "World", LessonPartType.TEXT)
    )

    val lessonWithWrongInput = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#000000", "World")
        ),
        Input(11, 45),
        true
    )

    val lessonPartsWithWrongInput = listOf(
        LessonPart("#000000", "Hello", LessonPartType.TEXT),
        LessonPart("#000000", "World", LessonPartType.TEXT)
    )

    val lessonWithInputOnTheBegin = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#FFFFFF", "World")
        ),
        Input(0, 3),
        true
    )

    val lessonPartsWithInputOnTheBegin = listOf(
        LessonPart("#000000", "Hell", LessonPartType.INPUT),
        LessonPart("#000000", "o", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "World", LessonPartType.TEXT)
    )

    val lessonWithInputOnTheBeginOfSecondContent = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#FFFFFF", "World")
        ),
        Input(5, 7),
        true
    )

    val lessonPartsWithInputOnTheBeginOfSecondContent = listOf(
        LessonPart("#000000", "Hello", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "Wor", LessonPartType.INPUT),
        LessonPart("#FFFFFF", "ld", LessonPartType.TEXT)
    )

    val lessonWithInputOnTheEnd = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#FFFFFF", "World")
        ),
        Input(2, 4),
        true
    )

    val lessonPartsWithInputOnTheEnd = listOf(
        LessonPart("#000000", "He", LessonPartType.TEXT),
        LessonPart("#000000", "llo", LessonPartType.INPUT),
        LessonPart("#FFFFFF", "World", LessonPartType.TEXT)
    )

    val lessonWithInputOnTheEndOfSecondContent = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#FFFFFF", "World")
        ),
        Input(7, 9),
        true
    )

    val lessonPartsWithInputOnTheEndOfSecondContent = listOf(
        LessonPart("#000000", "Hello", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "Wo", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "rld", LessonPartType.INPUT)
    )

    val lessonWithInputOnTheMiddle = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#FFFFFF", "World")
        ),
        Input(1, 3),
        true
    )

    val lessonPartsWithInputOnTheMiddle = listOf(
        LessonPart("#000000", "H", LessonPartType.TEXT),
        LessonPart("#000000", "ell", LessonPartType.INPUT),
        LessonPart("#000000", "o", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "World", LessonPartType.TEXT)
    )

    val lessonWithInputOnTheMiddleOfSecondContent = Lesson(
        1,
        listOf(
            Content("#000000", "Hello"),
            Content("#FFFFFF", "World")
        ),
        Input(6, 8),
        true
    )

    val lessonPartsWithInputOnTheMiddleOfSecondContent = listOf(
        LessonPart("#000000", "Hello", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "W", LessonPartType.TEXT),
        LessonPart("#FFFFFF", "orl", LessonPartType.INPUT),
        LessonPart("#FFFFFF", "d", LessonPartType.TEXT)
    )
}