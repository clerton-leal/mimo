package com.example.mimo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mimo.db.AnswerDao
import com.example.mimo.mock.LessonsMocks
import com.example.mimo.network.LessonNetwork
import com.example.mimo.viewmodel.MimoWelcomeViewModel
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MimoWelcomeViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MimoWelcomeViewModel
    private lateinit var network: LessonNetwork
    private lateinit var answerDao: AnswerDao

    @Before
    fun setUp() {
        network = mockk()
        answerDao = mockk()
        every { answerDao.getSuccessAnswers() } returns listOf()
        viewModel = MimoWelcomeViewModel(network, answerDao)
    }

    @Test
    fun `when load lessons return error should show correct error message`() {
        every { network.getPerson() } returns Single.error(Throwable("Error"))

        viewModel.loadLessons()

        assertNull(viewModel.showLessonsSuccess().value)
        assertNull(viewModel.showRespondedAllLessons().value)
        assertNull(viewModel.showLesson().value)
        assertEquals("Error", viewModel.errorMessage().value)
    }

    @Test
    fun `when load lessons return that lessons are all responded should show all responded message`() {
        every { network.getPerson() } returns Single.just(LessonsMocks.allLessonsAreResponded)

        viewModel.loadLessons()

        assertNull(viewModel.showLessonsSuccess().value)
        assertTrue(viewModel.showRespondedAllLessons().value!!)
        assertNull(viewModel.showLesson().value)
        assertNull(viewModel.errorMessage().value)
    }

    @Test
    fun `when load lessons return that one lesson is responded should show the next lesson`() {
        every { network.getPerson() } returns Single.just(LessonsMocks.firstLessonsIsResponded)

        viewModel.loadLessons()

        assertNull(viewModel.showLessonsSuccess().value)
        assertNull(viewModel.showRespondedAllLessons().value)
        assertEquals(LessonsMocks.firstLessonsIsResponded.lessons[1], viewModel.showLesson().value)
        assertNull(viewModel.errorMessage().value)
    }

    @Test
    fun `when load lessons return that none lesson is responded should show the first lesson`() {
        every { network.getPerson() } returns Single.just(LessonsMocks.allLessonsAreNotResponded)

        viewModel.loadLessons()

        assertNull(viewModel.showLessonsSuccess().value)
        assertNull(viewModel.showRespondedAllLessons().value)
        assertEquals(LessonsMocks.allLessonsAreNotResponded.lessons[0], viewModel.showLesson().value)
        assertNull(viewModel.errorMessage().value)
    }

    @Test
    fun `when the first lesson is responded should show the second lesson`() {
        every { network.getPerson() } returns Single.just(LessonsMocks.allLessonsAreNotResponded)

        viewModel.loadLessons()
        viewModel.respondLesson(LessonsMocks.allLessonsAreNotResponded.lessons[0].apply {
            responded = true
        })

        assertNull(viewModel.showLessonsSuccess().value)
        assertNull(viewModel.showRespondedAllLessons().value)
        assertEquals(LessonsMocks.allLessonsAreNotResponded.lessons[1], viewModel.showLesson().value)
        assertNull(viewModel.errorMessage().value)
    }

    @Test
    fun `when the last lesson is responded should show the lessons success`() {
        every { network.getPerson() } returns Single.just(LessonsMocks.lastLessonsIsNotResponded)

        viewModel.loadLessons()
        viewModel.respondLesson(LessonsMocks.allLessonsAreNotResponded.lessons[2].apply {
            responded = true
        })

        assertTrue(viewModel.showLessonsSuccess().value!!)
        assertNull(viewModel.showRespondedAllLessons().value)
        assertEquals(LessonsMocks.allLessonsAreNotResponded.lessons[2], viewModel.showLesson().value)
        assertNull(viewModel.errorMessage().value)
    }
}