package com.example.mimo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mimo.db.Answer
import com.example.mimo.db.AnswerDao
import com.example.mimo.mock.LessonMocks
import com.example.mimo.viewmodel.LessonViewModel
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LessonViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var answerDao: AnswerDao
    private lateinit var viewModel: LessonViewModel

    @Before
    fun setUp() {
        answerDao = mockk()
        every { answerDao.insert(any()) } just Runs
        viewModel = LessonViewModel(answerDao)
    }

    @Test
    fun `when show lesson without input should return only texts`() {
        viewModel.showLesson(LessonMocks.lessonWithoutInput)

        assertEquals(LessonMocks.lessonPartsWithoutInput, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with wrong input should return only texts`() {
        viewModel.showLesson(LessonMocks.lessonWithWrongInput)

        assertEquals(LessonMocks.lessonPartsWithWrongInput, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with input on the begin should return one more part with input`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheBegin)

        assertEquals(LessonMocks.lessonPartsWithInputOnTheBegin, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with input on the begin of second content should return one more part with input`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheBeginOfSecondContent)

        assertEquals(LessonMocks.lessonPartsWithInputOnTheBeginOfSecondContent, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with input on the end should return one more part with input`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheEnd)

        assertEquals(LessonMocks.lessonPartsWithInputOnTheEnd, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with input on the end of second content should return one more part with input`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheEndOfSecondContent)

        assertEquals(LessonMocks.lessonPartsWithInputOnTheEndOfSecondContent, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with input on the middle should return two more part with input`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheMiddle)

        assertEquals(LessonMocks.lessonPartsWithInputOnTheMiddle, viewModel.showLesson().value)
    }

    @Test
    fun `when show lesson with input on the middle of second content should return two more part with input`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheMiddleOfSecondContent)

        assertEquals(LessonMocks.lessonPartsWithInputOnTheMiddleOfSecondContent, viewModel.showLesson().value)
    }

    @Test
    fun `when validate answer with success should show success page`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheMiddleOfSecondContent)
        every { answerDao.get(1) } returns Answer(1, 11122222)
        viewModel.validateResponse("orl")

        assertTrue(viewModel.correctResponse().value!!)
    }

    @Test
    fun `when validate answer with error should show error page`() {
        viewModel.showLesson(LessonMocks.lessonWithInputOnTheMiddleOfSecondContent)
        every { answerDao.get(1) } returns Answer(1, 11122222)
        viewModel.validateResponse("or")

        assertTrue(viewModel.errorResponse().value!!)
    }
}