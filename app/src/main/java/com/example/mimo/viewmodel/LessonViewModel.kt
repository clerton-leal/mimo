package com.example.mimo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mimo.db.Answer
import com.example.mimo.db.AnswerDao
import com.example.mimo.domain.*

class LessonViewModel(private val answerDao: AnswerDao): ViewModel() {

    private val showLesson = MutableLiveData<List<LessonPart>>()
    private val correctResponse = MutableLiveData<Boolean>()
    private val errorResponse = MutableLiveData<Boolean>()

    private var lessonId = 0L

    fun showLesson(lesson: Lesson) {
        lessonId = lesson.id
        showLesson.postValue(getLessonParts(lesson))
        answerDao.insert(Answer(lessonId, System.currentTimeMillis()))
    }

    fun showLesson() = showLesson as LiveData<List<LessonPart>>
    fun correctResponse() = correctResponse as LiveData<Boolean>
    fun errorResponse() = errorResponse as LiveData<Boolean>

    fun validateResponse(text: String?) {
        val input = showLesson.value?.find { it.type == LessonPartType.INPUT }
        if (input?.text == text) {
            correctResponse.postValue(true)
            answerDao.insert(answerDao.get(lessonId).apply {
                end = System.currentTimeMillis()
            })
        } else {
            errorResponse.postValue(true)
        }
    }

    private fun getLessonParts(lesson: Lesson): List<LessonPart> {
        val input = lesson.input
        return if (input == null) {
            lesson.content.map { LessonPart(it.color, it.text, LessonPartType.TEXT) }
        } else {
            val indexOfTheContentWithInput = getIndexOfTheContentWithInput(lesson, input)

            if (indexOfTheContentWithInput < 0) {
                return lesson.content.map { LessonPart(it.color, it.text, LessonPartType.TEXT) }
            }

            val partsBeforeInput = generatePartsBeforeInput(indexOfTheContentWithInput, lesson)
            val inputParts = generateInputParts(indexOfTheContentWithInput, input, lesson)
            val partsAfterInput = generatePartsAfterInput(indexOfTheContentWithInput, lesson)

            partsBeforeInput + inputParts + partsAfterInput
        }
    }

    private fun generatePartsBeforeInput(
        indexOfTheContentWithInput: Int,
        lesson: Lesson
    ): List<LessonPart> {
        return lesson.content.filterIndexed { index, _ -> index < indexOfTheContentWithInput }
            .map { LessonPart(it.color, it.text, LessonPartType.TEXT) }
    }

    private fun generateInputParts(
        indexOfTheContentWithInput: Int,
        input: Input,
        lesson: Lesson
    ): List<LessonPart> {
        val inputParts = mutableListOf<LessonPart>()
        val contentWithInput = lesson.content[indexOfTheContentWithInput]

        val inputRangeInsideThisContent = getInputRangeInsideThisContent(indexOfTheContentWithInput, input, lesson)

        val textBeforeInput = getTextBeforeInput(contentWithInput, inputRangeInsideThisContent)
        if (!textBeforeInput.isNullOrEmpty()) {
            inputParts.add(LessonPart(contentWithInput.color, textBeforeInput, LessonPartType.TEXT))
        }

        val inputText = getInputText(contentWithInput, inputRangeInsideThisContent)
        inputParts.add(LessonPart(contentWithInput.color, inputText, LessonPartType.INPUT))

        val textAfterInput = getTextAfterInput(contentWithInput, inputRangeInsideThisContent)
        if (!textAfterInput.isNullOrEmpty()) {
            inputParts.add(LessonPart(contentWithInput.color, textAfterInput, LessonPartType.TEXT))
        }

        return inputParts
    }

    private fun getTextAfterInput(
        contentWithInput: Content,
        inputRange: Pair<Int, Int>
    ): String? {
        return if (inputRange.second == contentWithInput.text.length) {
            null
        } else {
            contentWithInput.text.substring(inputRange.second + 1, contentWithInput.text.length)
        }
    }

    private fun getInputText(
        contentWithInput: Content,
        inputRange: Pair<Int, Int>
    ): String {
        return contentWithInput.text.substring(inputRange.first, inputRange.second + 1)
    }

    private fun getInputRangeInsideThisContent(
        contentIndexWithInput: Int,
        input: Input,
        lesson: Lesson
    ): Pair<Int, Int> {
        var startInputIndex = input.startIndex
        var endInputIndex = input.endIndex

        lesson.content.forEachIndexed { index, content ->
            if (index < contentIndexWithInput) {
                startInputIndex -= content.text.length
                endInputIndex -= content.text.length
            }
        }

        return startInputIndex to endInputIndex
    }

    private fun getTextBeforeInput(contentWithInput: Content, inputRange: Pair<Int, Int>): String? {
        return if (inputRange.first == 0) {
            null
        } else {
            contentWithInput.text.substring(0, inputRange.first)
        }
    }

    private fun generatePartsAfterInput(
        indexOfTheContentWithInput: Int,
        lesson: Lesson
    ): List<LessonPart> {
        return lesson.content.filterIndexed { index, _ -> index > indexOfTheContentWithInput }
            .map { LessonPart(it.color, it.text, LessonPartType.TEXT) }
    }

    private fun getIndexOfTheContentWithInput(lesson: Lesson, input: Input): Int {
        var currentWordIndex = -1
        lesson.content.forEachIndexed { index, content ->
            currentWordIndex += content.text.length
            if (currentWordIndex >= input.startIndex) {
                return index
            }
        }

        return -1
    }
}