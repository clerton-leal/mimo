package com.example.mimo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mimo.db.AnswerDao
import com.example.mimo.domain.Lesson
import com.example.mimo.domain.Lessons
import com.example.mimo.network.LessonNetwork

class MimoWelcomeViewModel(private val lessonNetwork: LessonNetwork, private val answerDao: AnswerDao): ViewModel() {

    private val showLesson = MutableLiveData<Lesson>()
    private val showLessonsSuccess = MutableLiveData<Boolean>()
    private val showRespondedAllLessons = MutableLiveData<Boolean>()
    private val errorMessage = MutableLiveData<String>()

    private var lessons: List<Lesson>? = null

    fun loadLessons() {
        lessonNetwork.getPerson().subscribe({
            reduceLoadLessons(it)
        }, {
            reduceError(it)
        })
    }

    fun respondLesson(respondedLesson: Lesson) {
        lessons = lessons?.map  { currentLesson -> if (currentLesson == respondedLesson) respondedLesson else currentLesson }
        reduceRespondLesson(lessons)
    }

    fun showLesson() = showLesson as LiveData<Lesson>
    fun showLessonsSuccess() = showLessonsSuccess as LiveData<Boolean>
    fun showRespondedAllLessons() = showRespondedAllLessons as LiveData<Boolean>
    fun errorMessage() = errorMessage as LiveData<String>

    private fun reduceLoadLessons(responseLessons: Lessons) {
        val filledLessons = fillRespondedLessons(responseLessons)
        lessons = filledLessons

        if (allLessonsAreResponded(filledLessons)) {
            showRespondedAllLessons.postValue(true)
        } else {
            val nextLesson = getNextLesson(filledLessons)
            showLesson.postValue(nextLesson)
        }
    }

    private fun reduceRespondLesson(lessons: List<Lesson>?) {
        lessons?.let {
            if (allLessonsAreResponded(it)) {
                showLessonsSuccess.postValue(true)
            } else {
                val nextLesson = getNextLesson(it)
                showLesson.postValue(nextLesson)
            }
        }
    }

    private fun reduceError(error: Throwable) {
        errorMessage.postValue(error.message)
    }

    private fun fillRespondedLessons(lessons: Lessons): List<Lesson> {
        val answersIds = answerDao.getSuccessAnswers().map { it.id }
        return lessons.lessons.map { if (answersIds.contains(it.id)) it.apply { responded = true } else it }
    }

    private fun getNextLesson(lessons: List<Lesson>): Lesson {
        return lessons.first { !it.responded }
    }

    private fun allLessonsAreResponded(lessons: List<Lesson>): Boolean {
        return lessons.all { it.responded }
    }

    fun deleteProgress() {
        answerDao.deleteAll()
    }
}