package com.example.mimo.injection

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.mimo.MimoRouter
import com.example.mimo.db.MimoDatabase
import com.example.mimo.network.LessonNetwork
import com.example.mimo.network.RetrofitProvider
import com.example.mimo.viewmodel.LessonViewModel
import com.example.mimo.viewmodel.MimoWelcomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object Module {

    fun getModule() = module {
        single {
            RetrofitProvider.getRetrofit()
        }

        factory {
            val retrofit: Retrofit = get(Retrofit::class)
            retrofit.create(LessonNetwork::class.java)
        }

        viewModel {
            MimoWelcomeViewModel(get(), get())
        }

        viewModel {
            LessonViewModel(get())
        }

        factory { (activity: AppCompatActivity) ->
            MimoRouter(activity)
        }

        single {
            val mimoDatabase: MimoDatabase = Room.databaseBuilder(
                get(),
                MimoDatabase::class.java, "mimo.db"
            ).allowMainThreadQueries().build()
            mimoDatabase
        }

        factory {
            val database: MimoDatabase = get()
            database.answerDao()
        }
    }

}