package com.example.mimo

import android.app.Activity
import android.content.Intent
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.mimo.domain.Lesson
import com.example.mimo.view.LessonActivity

const val LESSON = "LESSON"

class MimoRouter(private val activity: AppCompatActivity) {

    private var resultLauncher: ActivityResultLauncher<Intent>? = null

    fun registerLessonCallback(successCallback: (Lesson?) -> Unit) {
        resultLauncher = activity.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                successCallback(data?.getParcelableExtra(LESSON))
            }
        }
    }

    fun navigateToLesson(lesson: Lesson) {
        val intent = Intent(activity, LessonActivity::class.java)
        intent.putExtra(LESSON, lesson)
        resultLauncher?.launch(intent)
    }

    fun navigateToWelcome(lesson: Lesson?) {
        val intent = Intent()
        intent.putExtra(LESSON, lesson)
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }

}