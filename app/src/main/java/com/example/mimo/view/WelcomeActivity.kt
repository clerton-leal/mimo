package com.example.mimo.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.mimo.MimoRouter
import com.example.mimo.R
import com.example.mimo.databinding.ActivityWelcomeBinding
import com.example.mimo.viewmodel.MimoWelcomeViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class WelcomeActivity : AppCompatActivity() {

    private val viewModel by viewModel<MimoWelcomeViewModel>()
    private val router by inject<MimoRouter> { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener {
            viewModel.loadLessons()
        }

        router.registerLessonCallback { lesson ->
            lesson?.let {
                viewModel.respondLesson(lesson)
            }
        }

        viewModel.showLesson().observe(this) {
            router.navigateToLesson(it)
        }

        viewModel.showLessonsSuccess().observe(this) {
            binding.button.visibility = View.GONE
            binding.textView.text = getString(R.string.congratulations)
        }

        viewModel.errorMessage().observe(this) {
            binding.button.visibility = View.GONE
            binding.textView.text = it
        }

        viewModel.showRespondedAllLessons().observe(this) {
            binding.textView.text = getString(R.string.retry)
            binding.button.text = getString(R.string.delete_progress)
            binding.button.setOnClickListener {
                viewModel.deleteProgress()
                binding.textView.text = getString(R.string.welcome_to_miro)
                binding.button.text = getString(R.string.start)
                binding.button.setOnClickListener {
                    viewModel.loadLessons()
                }
            }
        }
    }

}