package com.example.mimo.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import com.example.mimo.MimoRouter
import com.example.mimo.databinding.ActivityLessonBinding
import com.example.mimo.viewmodel.LessonViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import android.widget.TextView

import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import com.example.mimo.LESSON
import com.example.mimo.R
import com.example.mimo.domain.Lesson
import com.example.mimo.domain.LessonPartType


class LessonActivity : AppCompatActivity() {

    private val viewModel by viewModel<LessonViewModel>()
    private val router by inject<MimoRouter> { parametersOf(this) }

    private var input: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityLessonBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val lesson = intent.getParcelableExtra<Lesson>(LESSON)

        viewModel.showLesson().observe(this) {
            it.forEach { lessonPart ->
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )

                val view = if (lessonPart.type == LessonPartType.TEXT) {
                    val textView = TextView(this)
                    textView.text = lessonPart.text
                    textView
                } else {
                    val editText = EditText(this)
                    input = editText
                    editText
                }

                view.layoutParams = params
                binding.linear.addView(view, params)
            }
        }

        viewModel.correctResponse().observe(this) {
            AlertDialog.Builder(this)
                .setMessage(R.string.congratulations_lesson)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                    router.navigateToWelcome(lesson?.apply {
                        responded = true
                    })
                }.create().show()
        }

        viewModel.errorResponse().observe(this) {
            AlertDialog.Builder(this)
            .setMessage(R.string.wrong)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }.create().show()
        }

        binding.runButton.setOnClickListener {
            viewModel.validateResponse(input?.text?.toString())
        }

        lesson?.let {
            viewModel.showLesson(lesson)
        }

    }

}