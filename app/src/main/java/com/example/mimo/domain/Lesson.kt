package com.example.mimo.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Parcelize
@Serializable
data class Lesson(
    val id: Long,
    val content: List<Content>,
    val input: Input? = null,
    @Transient var responded: Boolean = false
): Parcelable {
    override fun equals(other: Any?) = other is Lesson && other.id == this.id

    override fun hashCode() = id.hashCode()
}
