package com.example.mimo.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class Content(
    val color: String,
    val text: String
): Parcelable