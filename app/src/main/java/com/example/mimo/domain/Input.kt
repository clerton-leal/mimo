package com.example.mimo.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Parcelize
@Serializable
data class Input(
    val startIndex: Int,
    val endIndex: Int
): Parcelable
