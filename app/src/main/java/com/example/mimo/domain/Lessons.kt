package com.example.mimo.domain

import kotlinx.serialization.Serializable

@Serializable
data class Lessons(
    val lessons: List<Lesson>
)
