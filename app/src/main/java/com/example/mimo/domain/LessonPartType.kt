package com.example.mimo.domain

enum class LessonPartType {
    TEXT, INPUT
}
