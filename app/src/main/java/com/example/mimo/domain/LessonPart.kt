package com.example.mimo.domain

data class LessonPart(
    val color: String,
    val text: String,
    val type: LessonPartType
)
