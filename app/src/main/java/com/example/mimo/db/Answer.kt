package com.example.mimo.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Answer(
    @PrimaryKey val id: Long,
    val start: Long,
    var end: Long? = null
)
