package com.example.mimo.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AnswerDao {

    @Query("SELECT * FROM answer WHERE `end` IS NOT NULL")
    fun getSuccessAnswers(): List<Answer>

    @Query("SELECT * FROM answer WHERE id LIKE :id")
    fun get(id: Long): Answer

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: Answer)

    @Query("DELETE FROM answer")
    fun deleteAll()

}