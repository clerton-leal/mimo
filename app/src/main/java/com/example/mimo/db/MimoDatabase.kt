package com.example.mimo.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Answer::class), version = 1)
abstract class MimoDatabase : RoomDatabase() {
    abstract fun answerDao(): AnswerDao
}