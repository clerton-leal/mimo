package com.example.mimo.network

import com.example.mimo.domain.Lessons
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface LessonNetwork {

    @GET("lessons")
    fun getPerson(): Single<Lessons>


}